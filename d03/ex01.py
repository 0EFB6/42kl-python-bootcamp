#!/usr/bin/env python3

import os
import discord

my_secret = 'MTA5ODA4OTYyODgxNDI5OTE0OQ.GVCGFQ.jhf75u-SxbBPykaJolAeiaisXHSJ5FjDlgAzFo'
#my_secret = os.environ['MY_SECRET']

intents = discord.Intents.all()
intents.messages = True
client = discord.Client(intents=intents)

@client.event
async def on_ready():
    print(f'We have logged in as {client.user}')

@client.event
async def on_message(message):
    if message.author == client.user:
        return
    if message.content == '$hello':
        await message.channel.send('Hello!')
    if message.content == '$greet':
        await message.reply(f'Hello {message.author.name}')
    if message.content == '$greetings':
        await message.reply(f'Hello {message.author.nick}')
client.run(my_secret)
