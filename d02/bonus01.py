#!/usr/bin/env python3

from datetime import datetime
import pytz

local_timezone = pytz.timezone('Asia/Kuala_Lumpur')
local_time = datetime.now(local_timezone)
print(local_time.time())
