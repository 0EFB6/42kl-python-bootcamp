#!/usr/bin/env python3

age = {'Hans':24, 'Prag':23, 'Bunyo':18}
print(age)
print(age["Hans"])
age["Prag"] = 30
print(age["Prag"])
age.pop("Bunyo")
print(age)
